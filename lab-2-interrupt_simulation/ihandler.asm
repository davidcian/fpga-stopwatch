.equ    RAM, 0x1000
.equ    LEDs, 0x2000
.equ    TIMER, 0x2020
.equ    BUTTON, 0x2030

_start:
br main

interrupt_handler:

; save the registers to the stack

	addi 	sp, sp, -120
	stw 	ra, 0 (sp)
	stw 	at, 4 (sp)
	stw 	t0, 8(sp)
	stw 	t1, 12 (sp)
	stw 	t2, 16 (sp)
	stw 	t3, 20 (sp)
	stw 	t4, 24 (sp)
	stw 	t5, 28 (sp)
	stw 	t6, 32 (sp)
	stw 	t7, 36 (sp)
	stw 	s0, 40 (sp)
	stw 	s1, 44 (sp)
	stw 	s2, 48 (sp)
	stw 	s3, 52 (sp)

	add 	s0, zero, zero		; timer IRQ bit index = 0
	addi 	s1, zero, 2			; Buttons IRQ bit index = 2

	rdctl	t0, ipending	; t0 = ipendingvector
	add 	t1, zero, zero  ; t1 = 0 : irq bit

; look for the interrupt source
loop:
	andi	t2, t0, 1 	; select LSB
	beq		t2, zero, ipending_next_bit	; if bit is 0, must look at the next
	beq 	t1, s0, isr_timer
	beq		t1, s1, isr_buttons

ipending_next_bit:
	srli	t0, t0, 1	;shift ipending by one
	addi	t1, t1, 1	
	bne		t0, zero, loop
	
return_from_exception:

	ldw 	ra, 0 (sp)
	ldw 	at, 4 (sp)
	ldw 	t0, 8 (sp)
	ldw 	t1, 12 (sp)
	ldw 	t2, 16 (sp)
	ldw 	t3, 20 (sp)
	ldw 	t4, 24 (sp)
	ldw 	t5, 28 (sp)
	ldw 	t6, 32 (sp)
	ldw 	t7, 36 (sp)
	ldw 	s0, 40 (sp)
	ldw 	s1, 44 (sp)
	ldw 	s2, 48 (sp)
	ldw 	s3, 52 (sp)

	addi	sp, sp, 120

	addi ea, ea, -4

	eret



isr_timer:

	stw 	zero, TIMER (zero)

	ldw 	t3, LEDs+4 (zero)
	addi	t3, t3, 1
	stw 	t3, LEDs+4 (zero)

	br		ipending_next_bit


isr_buttons:

	addi	t5, zero, 1			; 1

	ldw 	t3, LEDs+8 (zero)	; load value

button_zero:
	ldw		t4, BUTTON+4 (zero)	; load for button 0
	andi	t4, t4, 1			; select bit for button 0
	beq		t4, t5, button_zero_incr 	;if equal 1 goto

button_one:

	ldw		t4, BUTTON+4(zero)	;load for button 1
	andi	t4, t4, 2
	srli	t4, t4, 1
	beq		t4, t5, button_one_decr

button_two:
	ldw 	t4, BUTTON+4 (zero)
	andi	t4, t4, 4
	srli 	t4, t4, 2
	beq 	t4, t5, button_two_incr_50

button_three:
	ldw 	t4, BUTTON+4 (zero)
	andi	t4, t4, 8
	srli	t4, t4, 3
	beq		t4, t5, button_three_incr_200

buttons_end:
	stw 	t3, LEDs+8 (zero)

	stw		zero, BUTTON+4 (zero)

	br		ipending_next_bit


button_zero_incr:
	addi	t3, t3, 1	;+1
	br 	button_one
	
button_one_decr:
	sub		t3, t3, t5	;-1
	br 	button_two		

button_two_incr_50:
	addi 	t3, t3, 50
	br 	button_three

button_three_incr_200:
	addi	t3, t3, 200
	br		buttons_end

main:

	addi	sp, zero, 0x1FFF

	addi	t7, zero, 1		; 1
	wrctl	status, t7		; set PIE to 1
	addi	t7, zero, 5		
	wrctl	ienable, t7 	; enable IRQs for bits 0 (timer) and 2 (buttons)
	
timer_set:
	addi 	t7, zero, 99 ; 100 cycles
	stw		t7, TIMER+8 (zero)

	addi	t7, zero, 7		;111
	stw 	t7, TIMER+4 (zero)	; set timer

	add		t6, zero, zero	; counter init
	


counter_one_loop:
	stw t6, LEDs (zero)
	addi t6, t6, 1
	br counter_one_loop