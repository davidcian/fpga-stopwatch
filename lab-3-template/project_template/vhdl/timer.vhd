library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity timer is
    port(
        -- bus interface
        clk     : in  std_logic;
        reset_n : in  std_logic;
        cs      : in  std_logic;
        read    : in  std_logic;
        write   : in  std_logic;
        address : in  std_logic_vector(1 downto 0);
        wrdata  : in  std_logic_vector(31 downto 0);

        irq     : out std_logic;
        rddata  : out std_logic_vector(31 downto 0)
    );
end timer;

-- the timer is implemented as a Mealy FSM (finite state machine)
-- the description is almost completely behavioral
architecture synth of timer is
	-- register file type
	type registerFile_type is array(0 to 3) of std_logic_vector(31 downto 0);
	-- actual register file
	signal registerFile: registerFile_type;
	-- next register inputs
	signal nextRegisterFile: registerFile_type;
	-- address flip-flop output
	signal addressOut: std_logic_vector(1 downto 0);
	-- read flip-flop output
	signal readOut: std_logic;
	-- cs flip-flop output
	signal csOut: std_logic;
begin
	-- synchronous process for counter state
	sync_proc: process(clk, reset_n)
	begin
		-- asynchronous reset (active low), does not depend on clock signal
		if (reset_n = '0') then
			registerFile(0) <= (others => '0');
			registerFile(1) <= (others => '0');
			registerFile(2) <= (others => '0');
			registerFile(3) <= (others => '0');
		-- change state on rising edge of clock
		elsif (rising_edge(clk)) then
			-- current state is updated with next state register file inputs
			registerFile <= nextRegisterFile;
			-- address, read, and cs signals go through flip-flops for correct bus protocol
			readOut <= read;
			csOut <= cs;
			addressOut <= address;
		end if;
	end process sync_proc;
	
	-- combinational process for combination state transition logic
	comb_proc: process(registerFile, write, clk)
	begin
		-- the next state input always starts out as the old one
		nextRegisterFile <= registerFile;
		-- if present state is started
		if (registerFile(0)(1) = '1') then
			-- if counter has reached 0
			if (to_integer(unsigned(registerFile(3))) = 0) then
				-- set TO bit to 1 to signal interrupt
				nextRegisterFile(0)(0) <= '1';
				-- reload counter register with value of period register
				nextRegisterFile(3) <= registerFile(2);
				-- if counter is in continuous mode (CONT bit set)
				if (registerFile(1)(1) = '1') then
				-- if counter is in count once mode (CONT bit not set)
				elsif (registerFile(1)(1) = '0') then
					-- set RUN bit to 0
					nextRegisterFile(0)(1) <= '0';
				end if;
			-- if counter has not reached 0
			else
				-- decrement counter value by 1
				nextRegisterFile(3) <= std_logic_vector(to_unsigned(to_integer(unsigned(registerFile(3))) - 1, 32));
			end if;
		-- if present state is stopped
		elsif (registerFile(0)(1) = '0') then
		end if;
		
		-- write sub-procedure (write has priority over other combinational logic)
		if (cs = '1' and write = '1') then
			-- if address is of status register
			if (address = "00") then
				-- if wrdata is 0, reset the TO bit to 0
				if (to_integer(unsigned(wrdata)) = 0) then
					nextRegisterFile(0)(0) <= '0';
				end if;
			end if;
			-- if address is of control register
			if (address = "01") then
				-- write data to control register
				nextRegisterFile(1) <= wrdata;
				-- if START bit is set
				if (wrdata(2) = '1') then
					-- set counter state to started (set RUN bit)
					nextRegisterFile(0)(1) <= '1';
				end if;
				-- if STOP bit is set
				if (wrdata(3) = '1') then
					-- set counter state to stopped (set RUN bit to 0)
					nextRegisterFile(0)(1) <= '0';
				end if;
			end if;
			-- if address is of period register
			if (address = "10") then
				-- write wrdata to period register
				nextRegisterFile(2) <= wrdata;
				-- load new period into counter register
				nextRegisterFile(3) <= wrdata;
				-- stop internal counter
				nextRegisterFile(0)(1) <= '0';
			end if;
			-- if address is of counter register
			if (address = "11") then
				-- counter register is read-only
			end if;
		end if;
	end process comb_proc;
	
	-- read process
	read_proc: process(csOut, readOut, addressOut, registerFile)
	begin
		-- if chip is selected (cs is asserted) and read is asserted
		if (csOut = '1' and readOut = '1') then 
			-- if address is of status register
			if (addressOut = "00") then
				rddata <= registerFile(0);
			end if;
			-- if address is of control register
			if (addressOut = "01") then
				-- START and STOP bits are write-only
				rddata <= registerFile(1) AND std_logic_vector(to_unsigned(3, 32));
			end if;
			-- if address is of period register
			if (addressOut = "10") then
				rddata <= registerFile(2);
			end if;
			-- if address is of counter register
			if (addressOut = "11") then
				rddata <= registerFile(3);
			end if;
		-- else if read is inactive, set it to high impedance
		else rddata <= (others => 'Z');
		end if;
	end process read_proc;
	-- an interrupt is requested (irq asserted) when timeout (TO) and interrupt timeout (ITO) bits are set
	irq <= registerFile(0)(0) AND registerFile(1)(0);
end synth;
