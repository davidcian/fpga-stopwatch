library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity PC is
    port(
        clk          : in  std_logic;
        reset_n      : in  std_logic;
        en           : in  std_logic;
        sel_a        : in  std_logic;
        sel_imm      : in  std_logic;
        sel_ihandler : in  std_logic;
        add_imm      : in  std_logic;
        imm          : in  std_logic_vector(15 downto 0);
        a            : in  std_logic_vector(15 downto 0);
        addr         : out std_logic_vector(31 downto 0)
    );
end PC;

architecture synth of PC is
	-- the value to be added to the PC (4 or imm)
   signal pcIncrementValue: std_logic_vector(15 downto 0);
	-- the size of a Nios II word, the standard PC increment value
	constant wordSize: std_logic_vector(15 downto 0) := std_logic_vector(to_unsigned(4, 16));
	-- the address of the interrupt handler
	constant iHandlerAddress: std_logic_vector(15 downto 0) := std_logic_vector(to_unsigned(4, 16));
	-- the imm is shifted left 2 bits for one addressing mode
	signal shiftedImm: std_logic_vector(15 downto 0);
	-- the group 1 addressing mode address (increment)
	signal sumAddress: std_logic_vector(15 downto 0);
	-- the selected group 2 addressing mode address (other modes)
	signal otherAddress: std_logic_vector(15 downto 0);
	-- signal is asserted if any group 2 addressing mode is active
	signal group2active: std_logic;
	-- current PC value stored in PC flip-flop
	signal currentPC: std_logic_vector(15 downto 0);
	-- next PC value to be stored in PC flip-flop
	signal nextPC: std_logic_vector(15 downto 0);
begin
	-- sequential process for the memory flip-flop storing the value of PC
	seq_proc: process(clk, reset_n)
	begin
		-- reset_n is active low
		if (reset_n = '0') then
			-- asynchronously reset the value of PC
			currentPC <= (others => '0');
		elsif (rising_edge(clk)) then
			-- if enable (en) signal is asserted
			if (en = '1') then
				currentPC <= nextPC;
			end if;
		end if;
	end process seq_proc;
	
	-- select the increment value with add_imm
	with add_imm select pcIncrementValue <=
		wordSize when '0',
		imm when '1',
		(others => '0') when others;
		
	-- shift imm left 2 bits for shiftedImm
	shiftedImm <= std_logic_vector(shift_left(unsigned(imm), 2));
	
	-- select group 2 addressing mode address
	group2select: process(a, shiftedImm, sel_a, sel_imm, sel_ihandler)
	begin
		if (sel_a = '1') then
			otherAddress <= a;
		elsif (sel_imm = '1') then
			otherAddress <= shiftedImm;
		elsif (sel_ihandler = '1') then
			otherAddress <= iHandlerAddress;
		end if;
	end process group2select;
	
	-- assert group2active if any group 2 addressing mode is active
	group2active <= sel_a OR sel_imm OR sel_ihandler;
	
	-- compute sum (increment) address (group 1 addressing modes)
	sumAddress <= std_logic_vector(to_unsigned(to_integer(unsigned(currentPC)) + to_integer(unsigned(pcIncrementValue)), 16));
	
	-- select next PC value
	with group2active select nextPC <=
		sumAddress when '0',
		otherAddress when '1',
		(others => '0') when others;
		
	-- extend PC value to fit address width (32 bits)
	addr(31 downto 16) <= (others => '0');
	addr(15 downto 0) <= currentPC;
end synth;
