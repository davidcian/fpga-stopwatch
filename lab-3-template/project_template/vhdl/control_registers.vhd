library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity control_registers is
    port(
        clk       : in  std_logic;
        reset_n   : in  std_logic;
        write_n   : in  std_logic;
        backup_n  : in  std_logic;
        restore_n : in  std_logic;
        address   : in  std_logic_vector(2 downto 0);
        irq       : in  std_logic_vector(31 downto 0);
        wrdata    : in  std_logic_vector(31 downto 0);

        ipending  : out std_logic;
        rddata    : out std_logic_vector(31 downto 0)
    );
end control_registers;

architecture synth of control_registers is
	-- the type of the register file
	type registerFileType is array(0 to 5) of std_logic_vector(31 downto 0);
	-- the actual register file
	signal registerFile: registerFileType;
	-- the inputs for the next register file values
	signal nextRegisterFile: registerFileType;
	-- separate signal for pseudo-register ipending
	signal ipending_reg: std_logic_vector(31 downto 0);
begin
	seq_proc: process(clk, reset_n)
	begin
		-- if reset_n is deasserted (it is active low)
		if (reset_n = '0') then
			registerFile(0) <= (others => '0');
			registerFile(1) <= (others => '0');
			registerFile(2) <= (others => '0');
			registerFile(3) <= (others => '0');
			registerFile(4) <= (others => '0');
			registerFile(5) <= (others => '0');
		elsif (rising_edge(clk)) then
			registerFile <= nextRegisterFile;
		end if;
	end process seq_proc;
	
	comb_proc: process(write_n, backup_n, restore_n, irq, wrdata, address, registerFile, ipending_reg)
	begin
		-- the next register inputs start out as the current state of the register file
		nextRegisterFile <= registerFile;
		
		-- write_n is active low
		if (write_n = '0') then
			-- if address is that of ipending (ipending is read-only)
			if (address = "100") then
			else nextRegisterFile(to_integer(unsigned(address))) <= wrdata;
			end if;
		end if;
		
		-- backup_n is active low
		if (backup_n = '0') then
			-- save PIE bit in EPIE bit
			nextRegisterFile(1)(0) <= registerFile(0)(0);
			-- clear PIE bit (set to 0)
			nextRegisterFile(0)(0) <= '0';
		end if;
		
		-- restore_n is active low
		if (restore_n = '0') then
			-- restore PIE bit from EPIE bit
			nextRegisterFile(0)(0) <= registerFile(1)(0);
		end if;
		
		-- assert ipending output signal if PIE bit is set and an interruption is pending (ipending register is not 0)
		if (registerFile(0)(0) = '1' and ipending_reg /= (ipending_reg'range => '0')) then
			ipending <= '1';
		else ipending <= '0';
		end if;
		
		-- update pseudo-register ipending with irq masked with the value of register ienable
		ipending_reg <= irq AND registerFile(3);
	end process comb_proc;
	
	-- the signal rddata asynchronously reads the register at the selected address
	with address select rddata <=
		registerFile(0) when "000",
		registerFile(1) when "001",
		registerFile(2) when "010",
		registerFile(3) when "011",
		ipending_reg when "100",
		registerFile(5) when "101",
		(others => 'Z') when others;
end synth;
